const mongoose = require('mongoose');

const departementsSchema = new mongoose.Schema({
    code: String,
    superviseur: String,
    dep: String
});

module.exports = mongoose.model('departement', departementsSchema);
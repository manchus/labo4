const mongoose = require('mongoose');

const etudentsSchema = new mongoose.Schema({
    code: String,
    nom: String,
    prenom: String,
    dep: String
});

module.exports = mongoose.model('etudent', etudentsSchema);
const mongoose = require('mongoose');

const friandisesSchema = new mongoose.Schema({
    compagnie: String,
    brand: String,
    price: String
});

module.exports = mongoose.model('friandise', friandisesSchema);
const express = require('express') 
// express app
const app = express() 
const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);
const connection = mongoose.connection;
//mongo DB
const  bodyParser = require('body-parser');
//bodyParser nous permet de lire les objects JSON
const Student = require('./models/modEtudiant');
const Departement = require('./models/modDepartement');
const Friandise = require('./models/modFriandise');
//Nous importons le modèle de données 
//app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({extended:false}));

/**  Labo 6 install avant installer  Npm install cors */
const cors = require('cors');
app.use(cors());

app.set('views', './views');
// register view engine
app.set('view engine', 'ejs'); // Par defaut il cherche les views dans le dossier 'views'
app.use('/public', express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));


mongoose.connect('mongodb://localhost:27017/ecole',({useUnifiedTopology: true, useNewUrlParser: true }));
//mongoose.connect('mongodb://GutierrezE:GutierrezE@10.30.40.121:27017/GutierrezE',{ useUnifiedTopology: true, useNewUrlParser: true } );
connection.once('open',()=>{
  console.log('Connected to MongoDB');
});

app.get('/lireUtil', (req,res) =>{
  Student.find()
    .exec()  //se mettre en attente de la réponse de la BD.
    .then(etudiant => res.status(200).json(etudiant));
});

app.get('/lireDept', (req,res) =>{
  Departement.find()
    .exec()  //se mettre en attente de la réponse de la BD.
    .then(departement => res.status(200).json(departement));
});


//read only One
app.get('/lireUnUtil/:id', (req, res) => {
  const id = req.params.id; 
  Student.findById(id).then(Student=> res.json(Student))
  .catch(err => res.status(400).json('Error:'+err));
  });
  /** */
  app.get('/lireUnDept/:id', (req, res) => {
    const id = req.params.id; 
    Departement.findById(id).then(Departement=> res.json(Departement))
    .catch(err => res.status(400).json('Error:'+err));
    });
    /** */
  
  app.post('/ajoutUtil',(req, res) =>{
    console.log('req.body', req.body);
   //nous créons premier l'objet JSON
    const etudiant3 = new Student(req.body);
    etudiant3.save((err, etudiant3)=>{
      if(err){
        return res.status(500).json(err);
      }
      res.status(201).json(etudiant3);
    });
  });
  
  app.post('/ajoutDept',(req, res) =>{
    console.log('req.body', req.body);
   //nous créons premier l'objet JSON
    const dept = new Departement(req.body);
    dept.save((err, dept)=>{
      if(err){
        return res.status(500).json(err);
      }
      res.status(201).json(dept);
    });
  });

  app.post('/upUtil/:id',(req, res)=>{
    const util = req.params.id;
    console.log(util);
    console.log(req.body);
    Student.findById(util).then(Student=>{
      Student.code = req.body.code;
      Student.nom = req.body.nom;
      Student.prenom = req.body.prenom;
      Student.dep = req.body.dep;
      
      Student.save()
      .then(()=>res.json('Édition réussie !'))
      .catch(err => res.status(400).json('Error:'+err));
    })
    .catch(err => res.status(400).json('Error:'+err));
  });

  app.post('/upDept/:id',(req, res)=>{
    const util = req.params.id;
    console.log(util);
    console.log(req.body);
    Departement.findById(util).then(Departement=>{
      Departement.code = req.body.code;
      Departement.superviseur = req.body.superviseur;
      Departement.dep = req.body.dep;
      
      Departement.save()
      .then(()=>res.json('Édition réussie !'))
      .catch(err => res.status(400).json('Error:'+err));
    })
    .catch(err => res.status(400).json('Error:'+err));
  });
  

app.delete('/delUtil/:id', (req, res) => {
    const id = req.params.id;
  Student.findByIdAndDelete(id, (err, student) => {
    if(err){
      return res.status(500).json(err);
    }
    res.status(202).json({msg: `étudiant avec L'id ${student._id} supprimée`});
  });
});

app.delete('/delDept/:id', (req, res) => {
  const id = req.params.id;
Departement.findByIdAndDelete(id, (err, dept) => {
  if(err){
    return res.status(500).json(err);
  }
  res.status(202).json({msg: `Département avec L'id ${dept._id} supprimée`});
});
});



//UPDATE
app.put('/lireUpdUtil/:id', (req, res) => {
  const id = req.params.id; 
  const update = req.body;
  console.log('id:',id);
  Student.findByIdAndUpdate(id, update,  (err, student) => {
    if(err){
      return res.status(500).send({message:`La mise à jour du produit a échoué: ${err} `});
    }
    res.status(200).send({student: student });
  });
  });
  

/**** */

  app.listen(3437, ()=> { 
    console.log("j'écoute le port 3437!");
  });
  